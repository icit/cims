<html>

	<?php
	
		session_start();
		
		if(!isset($_SESSION['username'])) {
			header("Location: admin_login.php");
		}
	
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "cims";

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 

		$sql = "SELECT * FROM students";
		$result = $conn->query($sql);
	?>
	
	<body>
	
		<a href="logout.php">Logout</a>
		Students List
		
		<table border="1" style="width:100%">
		  <tr>
			<th>Firstname</th>
			<th>Lastname</th> 
			<th>Age</th>
		  </tr>
		  <tr>
			<td>Jill</td>
			<td>Smith</td> 
			<td>50</td>
		  </tr>
		  <tr>
			<td>Eve</td>
			<td>Jackson</td> 
			<td>94</td>
		  </tr>
		</table>
		<br/>
		<br/>
		<table border="1" style="width:100%">
		  <tr>
			<th>Id</th>
			<th>Firstname</th>
			<th>Lastname</th> 
			<th>Age</th>
		  </tr>
		
		<?php
			if ($result->num_rows > 0) {
				// output data of each row
				while($row = $result->fetch_assoc()) {
					
		?>
		
		
		  <tr>
			<td align="center"><?php echo $row["id"]; ?></td>
			<td align="center"><?php echo $row["firstname"]; ?></td>
			<td align="center"><?php echo $row["lastname"]; ?></td> 
			<td align="center"><?php echo $row["age"]; ?></td>
		  </tr>
		
					
		<?php			
				}
			} else {
				echo "0 results";
			}
			$conn->close();
		
		?>
		
		</table>
	</body>
</html>